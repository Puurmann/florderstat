<?php
//namespace OxidEsales\OxidSupport\OrderstatModule;

class fastlanestatistik extends fastlanestatistik_parent{

    public function __construct()
    {
        error_reporting(E_ALL);
        ini_set("error_reporting", true);
    }

    public function render()
    {
        $this->_mgGetStats();
        return parent::render();
    }

    public function days_in_month($month, $year)
    {
        // calculate number of days in a month
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

    protected function _mgGetStats()
    {
        // Tag
        $sTimestampFrom = date("Y-m-d")." 00:00:00";
        $sTimestampTill = date("Y-m-d")." 23:59:59";
        $this->_aViewData['mgstat_day'] = $this->_mgGetStatsFrom($sTimestampFrom, $sTimestampTill);

        // diesen Monat
        $sTimestampFrom = date("Y-m")."-1 00:00:00";
        $sTimestampTill = date("Y-m-d H:i:s", mktime(23, 59, 59, ( date("m") + 1 ), 0, date("Y")));
        $this->_aViewData['mgstat_month'] = $this->_mgGetStatsFrom($sTimestampFrom, $sTimestampTill);

        // letzten Monat
        $lmonth = (intval(date("m"))-1);
        $sTimestampFrom = date("Y-").(intval(date("m"))-1)."-1 00:00:00";
        $sTimestampTill = date("Y-").(intval(date("m"))-1)."-".$this->days_in_month($lmonth,date("Y"))." 23:59:59";
        $this->_aViewData['mgstat_lmonth'] = $this->_mgGetStatsFrom($sTimestampFrom, $sTimestampTill);

        $aUserStat = oxRegistry::getConfig()->getRequestParameter("mgstat");

        if($aUserStat["csv"])
        {
            $this->csv($aUserStat);
        }

        $sSelect = "SELECT OXID, OXTITLE FROM oxmanufacturers ORDER BY OXTITLE";
        $result = oxDb::getDb()->getAll($sSelect);
        $list = array();
        $list[] = '<option value="0"></option>';
        foreach($result as $item)
        {
            $selected = (!empty($aUserStat["manufacturer"]) && $aUserStat["manufacturer"] == $item[0]) ? 'selected="selected"' : "";
            $list[] = '<option value="'.$item[0].'" '.$selected.'>'.$item[1].'</option>';
        }
        $this->_aViewData['mgstat_manufacturer_options'] = join("\n", $list);

        // Benutzereingabe
        if(!isset($aUserStat) || empty($aUserStat))
        {
            $sUserFrom = date("Y-").(intval(date("m")) - 1).date("-d");
            $sUserTill = date("Y-m-d");

            $aUserFrom = array("day" => date("d"), "month" => date("m") - 1, "year" => date("Y"));
            $aUserTill = array("day" => date("d"), "month" => date("m"), "year" => date("Y"));

            $this->_aViewData['mgstat_useredit_from'] = $aUserFrom;
            $this->_aViewData['mgstat_useredit_till'] = $aUserTill;

        }
        else
        {
            $sUserFrom = $aUserStat["from"]["year"]."-".$aUserStat["from"]["month"]."-".$aUserStat["from"]["day"];
            $sUserTill = $aUserStat["till"]["year"]."-".$aUserStat["till"]["month"]."-".$aUserStat["till"]["day"];

            $this->_aViewData['mgstat_useredit_from'] = $aUserStat["from"];
            $this->_aViewData['mgstat_useredit_till'] = $aUserStat["till"];
        }

        $manufacturer = empty($aUserStat["manufacturer"]) ? null : $aUserStat["manufacturer"];
        $sTimestampFrom = $sUserFrom." 00:00:00";
        $sTimestampTill = $sUserTill." 23:59:59";

        $this->_aViewData['mgstat_userdef'] = $this->_mgGetStatsFrom($sTimestampFrom, $sTimestampTill, $manufacturer);

        // Total
        $sTimestampFrom = "2000-01-01 00:00:00";
        $sTimestampTill = "2030-01-01 23:59:59";
        $this->_aViewData['mgstat_total'] = $this->_mgGetStatsFrom($sTimestampFrom, $sTimestampTill);

    }

    private function csv($aUserStat)
    {
        $oLang = oxNew('oxLang');
        $rows = [];
        $row = [
            $oLang->translateString('SUABOORDERSTAT_DATE'),
            $oLang->translateString('SUABOORDERSTAT_NET'),
            $oLang->translateString('SUABOORDERSTAT_BRUT')
        ];
        $rows[]= $row;

        $manufacturer = empty($aUserStat["manufacturer"]) ? null : $aUserStat["manufacturer"];
        $sUserFrom = $aUserStat["from"]["year"]."-".$aUserStat["from"]["month"]."-".$aUserStat["from"]["day"];
        $sUserTill = $aUserStat["till"]["year"]."-".$aUserStat["till"]["month"]."-".$aUserStat["till"]["day"];
        $sFrom = $sUserFrom." 00:00:00";
        $sTill = $sUserTill." 23:59:59";

//        $sSqlFilter = "UNIX_TIMESTAMP(oxorderdate) >= UNIX_TIMESTAMP('$sFrom') AND UNIX_TIMESTAMP(oxorderdate) <= UNIX_TIMESTAMP('$sTill') AND UNIX_TIMESTAMP(OXSENDDATE) > UNIX_TIMESTAMP('0000-00-00 00:00:00')";
        $sSqlFilter = "oxorderdate >= '$sFrom' AND oxorderdate <= '$sTill' AND OXSENDDATE > '0000-00-00 00:00:00'";

        if($manufacturer)
        {
            $sSqlFilter.= " ";
            $sSelect = "SELECT oxbrutprice, oxnetprice , oxorder.oxorderdate, oxorder.oxid FROM oxorderarticles LEFT JOIN oxorder ON(oxorder.oxid=oxorderarticles.oxorderid) LEFT JOIN oxarticles ON(oxorderarticles.oxartid=oxarticles.oxid) WHERE $sSqlFilter AND oxarticles.OXMANUFACTURERID='".$manufacturer."'";
            $aArticleSumList = oxDb::getDb()->getAll($sSelect);
            foreach($aArticleSumList as $item)
            {
                $row = [$item[2] , $item[1] , $item[0]];
                if(empty($rows[$item[3]]))
                {
                    $rows[$item[3]] = $row;
                }
                else
                {
                    $rows[$item[3]][1]+= $item[1];
                    $rows[$item[3]][2]+= $item[0];
                }
            }
        }
        else
        {
            $sSelect = "SELECT oxtotalbrutsum, oxtotalnetsum, oxorderdate, oxdelcost FROM oxorder WHERE $sSqlFilter";
            $aArticleSumList = oxDb::getDb()->getAll($sSelect);
            foreach($aArticleSumList as $item)
            {
                $row = [$item[2] , $item[1] , $item[0]];
                $rows[]= $row;
            }
        }

        $fileName = "statistics.csv";
        ob_clean();
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=' . $fileName);

        $handle = fopen('php://output', 'w');
        foreach ($rows as $key => $line)
        {
            if($key)
            {
                $line[0] = date("d.m.Y" , strtotime($line[0]));
                $line[1] = number_format($line[1], 2);
                $line[2] = number_format($line[2], 2);
            }
            fputcsv($handle, $line, ",");
        }
        fclose($handle);
        exit();
    }

    protected function _mgGetStatsFrom($sFrom, $sTill, $manufacturer=null)
    {
        //$sSqlFilter = "UNIX_TIMESTAMP(oxorderdate) >= UNIX_TIMESTAMP('$sFrom') AND UNIX_TIMESTAMP(oxorderdate) <= UNIX_TIMESTAMP('$sTill') AND UNIX_TIMESTAMP(OXSENDDATE) > UNIX_TIMESTAMP('0000-00-00 00:00:00')";
        $sSqlFilter = "oxorderdate >= '$sFrom' AND oxorderdate <= '$sTill' AND OXSENDDATE > '0000-00-00 00:00:00'";
        //oxorder.OXSENDDATE
        // Anzahl Bestellungen
        $iOrderCnt = $this->_mgGetOrderCount($sSqlFilter);

        // Warenwert
        $aArticleSum = $this->_mgGetArticleSum($sSqlFilter, $manufacturer);

        // Gesamtwert
        $fGesamtBrutto = number_format($aArticleSum["fTotalBrutto"] + $aArticleSum["fShipping"], 2, ".", "");
        $fGesamtNetto  = number_format($aArticleSum["fTotalNetto"] + ($aArticleSum["fShipping"] * 0.81), 2, ".", "");
        $aGesamt = array($fGesamtBrutto, $fGesamtNetto);

        return array("iOrderCnt"    => $iOrderCnt,
            "aArticleSum"  => $aArticleSum,
            "aGesamt"      => $aGesamt);
    }

    protected function _mgGetOrderCount($sSqlFilter)
    {
        $sSelect = "SELECT COUNT(oxid) FROM oxorder WHERE $sSqlFilter";
        $iOrderCnt = oxDb::getDb()->getOne($sSelect);

        return $iOrderCnt;
    }

    protected function _mgGetArticleSum($sSqlFilter, $manufacturer=null)
    {
        $fTotalBrutto = 0;
        $fTotalNetto  = 0;
        $fShipping    = 0;

        if($manufacturer)
        {
            $sSqlFilter.= " ";
            $sSelect = "SELECT oxbrutprice, oxnetprice FROM oxorderarticles LEFT JOIN oxorder ON(oxorder.oxid=oxorderarticles.oxorderid) LEFT JOIN oxarticles ON(oxorderarticles.oxartid=oxarticles.oxid) WHERE $sSqlFilter AND oxarticles.OXMANUFACTURERID='".$manufacturer."'";
            $aArticleSumList = oxDb::getDb()->getAll($sSelect);
        }
        else
        {
            $sSelect = "SELECT oxtotalbrutsum, oxtotalnetsum, oxdelcost FROM oxorder WHERE $sSqlFilter";
            $aArticleSumList = oxDb::getDb()->getAll($sSelect);
        }

        foreach($aArticleSumList as $aArticleSum) {
            $fTotalBrutto += $aArticleSum[0];
            $fTotalNetto += $aArticleSum[1];
            $fShipping += $aArticleSum[2];
        }

        return array("fTotalBrutto" => number_format($fTotalBrutto, 2, ".", ""),
            "fTotalNetto"  => number_format($fTotalNetto, 2, ".", ""),
            "fShipping"    => number_format($fShipping, 2, ".", "")
        );
    }
}
?>
