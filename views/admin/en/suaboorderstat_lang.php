<?php

$sLangName  = "English";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                    => 'UTF-8',
    'SUABOORDERSTAT_TODAY'       => 'Today',
    'SUABOORDERSTAT_THISMONTH'   => 'this month',
    'SUABOORDERSTAT_LASTMONTH'   => 'last month',
    'SUABOORDERSTAT_FROM'        => 'from',
    'SUABOORDERSTAT_TILL'        => 'till',
    'SUABOORDERSTAT_TOTAL'       => 'Total',
    'SUABOORDERSTAT_ORDERCNT'    => 'Amount of Orders',
    'SUABOORDERSTAT_ARTNETTO'    => 'Total article without tax',
    'SUABOORDERSTAT_ARTBRUTTO'   => 'Total article with tax',
    'SUABOORDERSTAT_DELCOST'     => 'Deliverycost',
    'SUABOORDERSTAT_TOTALBRUTTO' => 'Total with tax',
    'SUABOORDERSTAT_TOTALNETTO'  => 'Total without tax',
    'SUABOORDERSTAT_MANUFACTURER' => 'Manufacturer',
    'SUABOORDERSTAT_EXPORT'      => 'Export to CSV',
    'SUABOORDERSTAT_DATE'        => 'Datum',
    'SUABOORDERSTAT_NET'         => 'Total without tax',
    'SUABOORDERSTAT_BRUT'        => 'Total with tax'
);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE" }]
*/
