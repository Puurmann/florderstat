<?php

use OxidEsales\Eshop\Application\Controller\Admin\OrderOverview;

/**
 * Metadata version
 */
$sMetadataVersion = '1.1';

/**
 * Module information
 */
$aModule = [
    'id'          => 'florderstat',
    'title'       => 'Fastlane Revenues Module',
    'description' =>
        [
            'de' => 'Zeigt in der Bestellübersich, wenn noch keine Bestellung gewählt wurde eine ausführliche Bestellstatistik an.',
            'en' => 'Show an advanced statistic in the order overview admin panel.'
        ],
    'thumbnail'    => '../fastlane_300.png',
    'version'      => '1.0.0',
    'url'          => 'http://fastlane.de',
    'email'        => 'info@fastlane.de',
    'extend' =>
        [
//            OrderOverview::class => OxidEsales\OxidSupport\OrderstatModule\fastlanestatistik::class
               'order_overview' => 'fastlane/flOrderStat/models/fastlanestatistik'
        ],
    'blocks' =>
        [
            [
                'template' => 'order_overview.tpl',
                'block' => 'admin_order_overview_general',
                'file' => 'views/blocks/orderstatistik.tpl'
            ],
            [
                'template' => 'headitem.tpl',
                'block' => 'admin_headitem_inccss',
                'file' => 'views/blocks/orderstyles.tpl'
            ],
        ],
];
